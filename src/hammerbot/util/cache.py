import asyncio
import threading

class Cacheable:
    def __init__(self, co):
        self.co = co
        self.done = False
        self.result = None
        self.lock = asyncio.Lock()
    
    def __await__(self):
        with (yield from self.lock):
            if self.done:
                return self.result
            self.result = yield from self.co.__await__()
            self.done = True
            return self.result

class ThreadSafeCacheable:
    def __init__(self, co):
        self.co = co
        self.done = False
        self.result = None
        self.lock = threading.Lock()
    
    def __await__(self):
        while True:
            if self.done:
                return self.result
            if self.lock.acquire(blocking=False):
                self.result = yield from self.co.__await__()
                self.done = True
                return self.result
            else:
                yield from asyncio.sleep(0.005)

def cacheable(*x, thread_safe: bool = False):
    def wrapper(f):
        def wrapped(*args, **kwargs):
            r = f(*args, **kwargs)
            if (thread_safe):
                return ThreadSafeCacheable(r)
            else:
                return Cacheable(r)
        
        return wrapped
    
    if (x):
        return wrapper(*x)
    else:
        return wrapper

__all__ = \
[
    'Cacheable',
    'ThreadSafeCacheable',
    
    'cacheable',
]
