from typing import *

from dataclasses import dataclass, field

from hammerdraw.util import ConfigLoader, setup_logging, preload_fonts
from .bot import HammerBot
from .config import HammerBotConfig
from .util import Daemon

@dataclass
class HammerBotApp(Daemon):
    config: HammerBotConfig = field(default_factory=HammerBotConfig.default)
    bot: Optional[HammerBot] = field(default=None, init=False)
    pid_file: str = None
    
    def __post_init__(self):
        if (self.pid_file is None):
            self.pid_file = self.config.pid_file
    
    def setup(self):
        # During the daemonization, current working directory gets changed to the root ('/')
        # This will prevent is's side effects
        self.config = self.config.replace(hammerdraw=self.config.hammerdraw.with_root())
        setup_logging(self.config.hammerdraw.logging_config)
        ConfigLoader.load_configs(main_config=self.config.hammerdraw)
    
    def preload(self):
        preload_fonts(self.config.hammerdraw.text_drawer)
    
    def run(self):
        self.bot = HammerBot(self.config)
        self.bot.logger.info(f"Config file used: {self.config}")
        self.preload()
        self.bot.start()

__all__ = \
[
    'HammerBotApp',
]
