#!/usr/bin/env bash
set -eu

cd "$(dirname "$(realpath "$0")")";
source ./config.sh

set +u
source ${VIRTUAL_ENV_PATH}/bin/activate
set -u

# export PYTHON_APPLICATION_CONFIG_PATH=${CONFIG_DIR}/application.conf
# export LOG_CFG=${CONFIG_DIR}/logging.json

pushd ${DEST_DIR}/hammerdraw-bot
    python -m hammerbot run
popd
