import sys
from typing import Iterator

from hammerbot import HammerBotApp

def get_help() -> Iterator[str]:
    lines = \
    [
        f"Usage: {sys.argv[0]} [ COMMAND ]",
        '',
        "The following commands are supported:",
        " - run   | Runs application without daemonization. This is the default command.",
        " - start | Starts application as a service",
        " - stop  | Stops service",
        " - kill  | Kills application by pid",
        " - help  | Print this and exit",
    ]
    
    yield from lines

def main(command='run'):
    app = HammerBotApp()
    
    if (command == 'run'):
        app.setup()
        app.run()
    
    elif (command == 'start'):
        app.setup()
        app.start()
    
    elif (command == 'stop'):
        app.setup()
        app.stop()
    
    elif (command == 'kill'):
        app.setup()
        app.kill(app.pid_file)
    
    elif (command == 'help'):
        for line in get_help():
            print(line)
        exit(0)
    
    else:
        print(f"Unsupported command: {command!r}", file=sys.stderr)
        for line in get_help():
            print(line, file=sys.stderr)
        exit(1)

if (__name__ == '__main__'):
    main(*sys.argv[1:])
