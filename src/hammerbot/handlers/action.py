from typing import Optional

from hammerdraw.interfaces import IConfigurable

from hammerbot.api import AttachmentOptions
from hammerbot.config import HammerBotConfig, BotConfig
from hammerbot.util import ImageSearcher, LinkProvider

from .base import BaseHandler

def ActionHandler(config: HammerBotConfig, *args, **kwargs):
    if (config.bot.image_search_engine is not None):
        image_client = config.bot.image_search_engine.cls()
    else:
        image_client = None
    
    link_client = config.bot.link_provider.cls
    if (isinstance(link_client, type) and issubclass(link_client, IConfigurable)):
        link_client = link_client(config.bot)
    elif (isinstance(link_client, type) and issubclass(link_client, LinkProvider)):
        link_client = link_client()
    
    if (config.bot.filter is not None):
        parents = (config.bot.filter.cls, BaseHandler, config.bot.handler_class.cls)
    else:
        parents = (BaseHandler, config.bot.handler_class.cls)
    
    class _ActionHandler(*parents):
        attachment_options = AttachmentOptions(**config.bot.attachment_options)
        hammerdraw_config = config.hammerdraw
        image_searcher: Optional[ImageSearcher] = image_client
        link_provider: LinkProvider = link_client
        
    return _ActionHandler(config.bot, *args, **kwargs)

__all__ = \
[
    'ActionHandler',
]
