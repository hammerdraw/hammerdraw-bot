import re

from typing.re import *

from hammerdraw.modules.keystone import KeystoneCardCompiler
from ..api.hammerdraw_api import HammerDrawApiWithImage
from ..api.parser_api import ParserOptions

class KeystoneCardApi(HammerDrawApiWithImage[KeystoneCardCompiler]):
    logger_name = 'hammerbot.impl.keystone-card'
    topic: str = "StarCraft"
    regex: Pattern
    
    def __init__(self):
        super().__init__()
        
        self.regex = re.compile(rf'''(?P<name>.+)
(?P<rarity>.+)
(?P<race>[\w/\\]+) (?P<type>.+)
(?P<minerals>\d+) *[/ ] *(?P<gas>\d+)
(?:set: *(?P<set>[^\n]+)$
)?
(?P<effect>(?:.|\s)*)''', re.MULTILINE)
        self.parser_options = ParserOptions \
        (
            compiler = KeystoneCardCompiler,
            compiler_args = dict(config=self.hammerdraw_config),
            regex = self.regex,
            multiline_fields = [ 'effect' ],
            expand_fields = [ 'image' ],
            decapitalize_fields = [ 'type', 'rarity', 'race' ],
            numeric_fields = [ 'minerals', 'gas' ],
        )
    
    def _update(self, instance: KeystoneCardCompiler, **kwargs) -> KeystoneCardCompiler:
        instance.raw['autoAdjustScale'] = True
        if (instance.raw.get('set', None) is None):
            instance.raw['set'] = 'hammerdraw'
        return super()._update(instance, **kwargs)

__all__ = \
[
    'KeystoneCardApi',
]
