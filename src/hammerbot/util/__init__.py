
from .daemon import *
from .image_searcher import *
from .link_provider import *

__all__ = \
[
    *daemon.__all__,
    *image_searcher.__all__,
    *link_provider.__all__,
]

try:
    from .image_searcher_impl import *
except ImportError:
    from warnings import warn as _warn
    from typing import NewType as _NT
    
    _warn(ImportWarning(f"Feature 'autosearch' is not enabled, cannot import {ImageSearcher.__name__} implementations"))
else:
    __all__.extend(image_searcher_impl.__all__)

try:
    from .link_shorter import *
except ImportError:
    from warnings import warn as _warn
    from typing import NewType as _NT
    
    _warn(ImportWarning(f"Feature 'link-shorter' is not enabled, cannot import {LinkProvider.__name__} implementations"))
else:
    __all__.extend(link_shorter.__all__)

try:
    from .link_provider_basic import *
except ImportError:
    from warnings import warn as _warn
    from typing import NewType as _NT
    
    _warn(ImportWarning(f"Feature 'link-shorter' is not enabled, cannot import {LinkProvider.__name__} implementations"))
else:
    __all__.extend(link_provider_basic.__all__)
