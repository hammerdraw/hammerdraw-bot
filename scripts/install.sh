#!/usr/bin/env bash
set -eu

cd "$(dirname "$(realpath "$0")")";
source ./config.sh

function install-user()
{
    if (id -u ${USER} >/dev/null 2>&1)
    then echo "User ${USER} already exists"
    else useradd ${USER}
    fi
}

function install-packages()
{
    install -vm 755 -dD -o ${USER} -g ${USER} -- ${DEST_DIR}
    sudo -HE -u ${USER} -- bash ./install-internal.sh
    install -vm 755 -dD -o ${USER} -g ${USER} -- ${CONFIG_DIR}
}

function install-systemd()
{
    local unit_file="/etc/systemd/system/${UNIT_NAME}"
    install -vm 755 -o root -g root -- hammerbot.service ${unit_file}
    
    for repl in USER DEST_DIR
    do sed -i "s|{${repl}}|${!repl}|g" ${unit_file}
    done
    
    systemctl daemon-reload
    systemctl enable ${UNIT_NAME}
}

echo "Installing HammerDraw Bot version ${HAMMERBOT_VERSION}..."
install-user
install-packages
install-systemd
echo "Installation successful!"
