from hammerbot import BotConfig
from hammerdraw.interfaces import IConfigurable

from .link_provider import LinkProvider

class BasicLinkProvider(LinkProvider, IConfigurable[BotConfig]):
    """
    A `LinkProvider` implementation that does not shorten the links.
    """
    
    async def provide_external_link(self, url: str) -> str:
        return url
    
    async def provide_internal_link(self, uri: str) -> str:
        raise NotImplementedError("Internal links are not yet supported")


__all__ = \
[
    'BasicLinkProvider',
]
