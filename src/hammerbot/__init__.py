"""
.. include:: ../../README.md
"""

from collections import namedtuple

__title__ = 'hammerbot'
__author__ = 'Peter Zaitcev / USSX Hares'
__license__ = 'BSD 2-clause'
__copyright__ = 'Copyright 2019 Peter Zaitcev'
__version__ = '1.3.1'


VersionInfo = namedtuple('VersionInfo', 'major minor micro releaselevel serial')
version_info = VersionInfo(*__version__.split('.'), releaselevel='alpha', serial=0)

from .app import *
from .bot import *
from .config import *
from .handlers import *

__all__ = \
[
    'version_info',
    '__title__',
    '__author__',
    '__license__',
    '__copyright__',
    '__version__',
    *app.__all__,
    *bot.__all__,
    *config.__all__,
    *handlers.__all__,
]
