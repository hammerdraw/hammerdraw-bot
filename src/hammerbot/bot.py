from typing import *

from botter.api import *
from botter.api.handlers import EventHandler

from .config import HammerBotConfig
from .handlers import *

def HammerBot(config: HammerBotConfig, *args, **kwargs) -> Bot:
    
    class _HammerBot(Bot):
        implementation = config.bot.provider.cls
        token = config.bot.token
        
        event_handlers = \
        [
            *ALL_COMMAND_HANDLERS,
            ActionHandler(config),
            WhoAmIHandler,
        ]
        
        def add_event_handler(self, handler: Union[EventHandler, Type[EventHandler]]):
            if (isinstance(handler, type) and issubclass(handler, EventHandler) and issubclass(handler, BaseHandler)):
                handler = handler(config.bot)
            return super().add_event_handler(handler)
        
        def __init__(self, *_args, **_kwargs):
            super().__init__(*_args, **_kwargs)
            self.client.token = self.token
    
    return _HammerBot(*args, **kwargs)

__all__ = \
[
    'HammerBot',
]
