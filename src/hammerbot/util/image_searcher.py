from abc import ABC
from typing import *

from botter import BotError

class ImageSearcherError(BotError):
    message: str
    default_message_format: str = "Unable to fetch an image for query '{query}'."
    params: Dict[str, Any]
    query: str
    
    def __init__(self, message: str = None, *, query: str = 'undefined', **kwargs):
        kwargs['query'] = query
        self.message = message or self.default_message_format.format_map(kwargs)
        self.query = query
        self.params = kwargs
        
        super().__init__(self.message)

class ImageSearcher(ABC):
    async def make_search(self, query: str) -> bytes:
        """
        Makes the search request.
        
        Args:
            query: `str` A fraze to search
        
        Returns:
            `bytes`. A loaded HTML page with the search results.
        
        """
        
        pass
    def find_images_on_page(self, html: bytes) -> Iterator[str]:
        """
        Filters the loaded page (in bytes)
        and returns the string generator for image URLs
        
        Args:
            html: `bytes`: Loaded HTML page.
        
        Returns:
            `Iterator[str]`: An iterator over the images found.
        """
        
        pass
    
    async def download_image(self, url: str) -> Tuple[bytes, str]:
        """
        Downloads image without storing it to the file.
        Returns file content and the extension.
        
        Args:
            url: `str`. Url of the image to be downloaded.
        
        Returns:
            `Tuple[bytes, str]`. Returns file content and file extension.

        """
        
        pass
    
    async def get_image_links(self, query: str) -> List[str]:
        """
        Searches for the specified query in specified search engine,
        and returns all image links from the first page of the results.
        
        Args:
            query: `str`. A query to search images for.
        
        Returns:
            `List[str]`. Returns list of links.
        
        """
        pass
    
    async def get_random_image(self, query: str) -> Union[Tuple[bytes, str], Tuple[None, None]]:
        """
        Searches for the specified query in specified search engine,
        and returns a random image from the first page of the results.
        
        Args:
            query: `str`. A query to search images for.
        
        Returns:
            `Tuple[bytes, str]`. Returns file content and file extension.
        
        """
        
        pass

__all__ = \
[
    'ImageSearcher',
    'ImageSearcherError',
]
