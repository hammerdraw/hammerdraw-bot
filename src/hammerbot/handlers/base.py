from abc import ABC

from botter.api.handlers import *
from hammerdraw.interfaces import IConfigurable

from hammerbot.config import BotConfig

class BaseHandler(IConfigurable[BotConfig], EventHandler, ABC):
    @property
    def logger_name(self) -> str:
        return f'hammerbot.{self.config.provider.cls.name}.handler'
    
    async def handle_error(self, event: Event, error: Exception):
        if (isinstance(error, self.config.provider.cls.error_base_type)):
            self.logger.error(f"Got {self.config.provider.cls.name} exception: {error}")
        else:
            await super().handle_error(event, error)

class WhoAmIHandler(BaseHandler, StartHandler):
    async def handle_event(self, event: StartEvent):
        self.logger.info('Logged in as')
        self.logger.info(event.bot_user.display_name)
        self.logger.info(event.bot_user.id)
        self.logger.info('------')

__all__ = \
[
    'BaseHandler',
    'WhoAmIHandler',
]
