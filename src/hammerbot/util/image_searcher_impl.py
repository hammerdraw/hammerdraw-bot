import asyncio
import os
import random
import re
from dataclasses import dataclass, field
from functools import lru_cache
from typing import *
from urllib.parse import quote_plus, unquote_plus

from tornado.httpclient import AsyncHTTPClient, HTTPResponse, HTTPError

from botter.util import Logging
from .image_searcher import ImageSearcher, ImageSearcherError
from .cache import cacheable

CACHE_SIZE: int = 32
DISTRIBUTION_SIGMA: float = 10.0
MAX_GAUSSIAN_ATTEMPTS: int = 3
YANDEX_HOST = 'https://yandex.ru'

class YandexBanError(ImageSearcherError):
    default_message_format: str = "Unable to fetch an image for query '{query}': Yandex seems to have banned the search engine. Captcha link: {captcha_link}"
    captcha_link: str
    
    def __init__(self, *args, captcha_link: str, **kwargs):
        self.captcha_link = captcha_link
        super().__init__(*args, **kwargs)

@dataclass
class CachedImageSearcher(ImageSearcher):
    @lru_cache(CACHE_SIZE)
    @cacheable
    async def get_image_links(self, query: str) -> List[str]:
        r = await super().get_image_links(query)
        return list(r)
    
    def __hash__(self):
        return id(self)

@dataclass
class YandexImageSearcher(ImageSearcher, Logging):
    client: AsyncHTTPClient = field(default_factory=AsyncHTTPClient)
    logger_name: str = 'hammerbot.util.image-searcher'
    
    async def make_search(self, query: str) -> bytes:
        """
        Makes the search request in Yandex.
        
        Args:
            query: `str` A fraze to search
        
        Returns:
            `bytes`. A loaded HTML page with the search results.

        """
        
        url = f'{YANDEX_HOST}/images/search?text={quote_plus(query)}'
        self.logger.debug(f"Making images search: {url!r}...")
        try:
            resp: HTTPResponse = await self.client.fetch(url)
        except Exception:
            raise HTTPError(500)
        else:
            if (resp.code != 200):
                raise HTTPError(resp.code)
        self.logger.debug(f"{url!r}: {resp.code} {resp.reason}")
        return resp.body
    
    def find_images_on_page(self, html: bytes) -> Iterator[str]:
        self.logger.debug(f"Filtering response to find images...")
        regexp = re.compile(rb'/images/search\?pos=\d+&amp;img_url=([^&]+)&amp;')
        images = set()
        for result in regexp.finditer(html):
            img_url = unquote_plus(result.group(1).decode())
            if (img_url in images):
                continue
            
            images.add(img_url)
            yield img_url
    
    def get_image_extension(self, resp: HTTPResponse) -> str:
        """
        Tries to return the file extension basing on HTTP Response.
        It searches in:
          1. Content-Type response header
          2. Content-Disposition response header
          3. URL of the original request
        
        Args:
            resp: `tornado.httpclient.HTTPResponse`
        
        Returns:
            `str`. An expected file extension.
        
        """
        
        content_types = resp.headers.get_list('Content-Type')
        for content_type in content_types:
            for mime_type in content_type.split(';'):
                mime_type = mime_type.strip()
                if (mime_type.startswith('image/')):
                    _, _, m = mime_type.partition('/')
                    m = m.strip().strip('"')
                    if (m): return m
        
        content_dispositions = resp.headers.get_list('Content-Disposition')
        for content_disposition in content_dispositions:
            for content in content_disposition.split(';'):
                content = content.strip()
                if (content.startswith('filename=')):
                    _, _, f = content.partition('=')
                    _. _, f = f.strip().strip('"').rpartition('.')
                    if (f): return f
        
        basename: str = os.path.basename(resp.request.url.partition('?')[0])
        _, _, extension = basename.rpartition('.')
        return extension
    
    async def download_image(self, url: str) -> Tuple[bytes, str]:
        self.logger.debug(f"Downloading image: {url!r}")
        
        try:
            resp: HTTPResponse = await self.client.fetch(url)
        except Exception:
            raise HTTPError(500)
        else:
            if (resp.code != 200):
                raise HTTPError(resp.code)
        extension = self.get_image_extension(resp)
        
        return resp.body, extension
    
    async def get_image_links(self, query: str) -> List[str]:
        """
        Searches for the specified query in [Yandex.Images](https://images.yandex.ru),
        and returns all image links from the first page of the results.
        
        Args:
            query: `str`. A query to search images for.
        
        Returns:
            `List[str]`. Iterates over the image links.
        
        """
        
        self.logger.info(f"Searching a random image for {query!r}...")
        
        try:
            html = await self.make_search(query)
        except HTTPError as e:
            self.logger.warning(f"Cannot make request to Yandex: {e}")
            return [ ]
        else:
            results = list(self.find_images_on_page(html))
            if (not results):
                self.logger.warning("Noting found")
                return [ ]
            else:
                return results
    
    async def get_random_image(self, query: str) -> Union[Tuple[bytes, str], Tuple[None, None]]:
        """
        Searches for the specified query in [Yandex.Images](https://images.yandex.ru),
        and returns a random image from the first page of the results.
        
        Args:
            query: `str`. A query to search images for.
        
        Returns:
            `Tuple[bytes, str]`. Returns file content and file extension.
        
        """
        
        results = list(await self.get_image_links(query))
        if (not results):
            return None, None
        
        for _ in range(MAX_GAUSSIAN_ATTEMPTS):
            expected = int(abs(random.gauss(0, DISTRIBUTION_SIGMA)))
            if (expected < len(results)):
                url = results[expected]
                try:
                    result = await self.download_image(url)
                except HTTPError:
                    self.logger.warning(f"Cannot download image: {url!r}")
                    del results[expected]
                else:
                    self.logger.info(f"Image successfully downloaded: {url!r}")
                    return result
        
        random.shuffle(results)
        for url in results:
            try:
                result = await self.download_image(url)
            except HTTPError:
                self.logger.warning(f"Cannot download image: {url!r}")
            else:
                self.logger.info(f"Image successfully downloaded: {url!r}")
                return result
        
        return None, None

@dataclass
class YandexCachedImageSearcher(CachedImageSearcher, YandexImageSearcher):
    pass

    def __hash__(self):
        return id(self)


__all__ = \
[
    'CachedImageSearcher',
    'YandexBanError',
    'YandexCachedImageSearcher',
    'YandexImageSearcher',
]

async def main():
    query = 'Aldaris, Baneling'
    content, extension = await YandexImageSearcher().get_random_image(query)
    path = os.path.abspath(f'image.{extension}')
    with open(path, 'wb') as f:
        f.write(content)
    
    print(f"Image downloaded and stored in {path!r}")
    
    return 0

if (__name__ == '__main__'):
    exit_code = asyncio.run(main())
    exit(exit_code)
