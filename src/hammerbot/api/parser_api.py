from logging import Logger
from typing import *
from typing import Pattern
from urllib import parse as urlparse

from dataclasses import dataclass, fields, Field

from botter.api import BotError
from hammerdraw.compilers import CompilerBase
from hammerdraw.util.text_to_raw import *

class ParserError(BotError):
    base_exception: TextToRawException
    message: str
    link: str
    def __init__(self, exception: TextToRawException, link: str = None):
        self.base_exception = exception
        self.message = f"Invalid data: {exception}."
        self.link = link
        if (link):
            self.message += ' ' f"You can try your data via the regex101 website, following by this link:" '\n' + link
        super().__init__(self.message)

T = TypeVar('T', bound=CompilerBase)
@dataclass(frozen=True)
class ParserOptions(Generic[T]):
    compiler: Type[T]
    regex: Pattern[str]
    compiler_args: Dict[str, Any] = None
    
    multiline_fields: List[str] = None
    decapitalize_fields: List[str] = None
    numeric_fields: List[str] = None
    expand_fields: List[str] = None
    list_fields: Dict[str, str] = None
    structure_fields: Dict[str, Tuple[str, List[str]]] = None
    update_fields: Union[Callable[[CompilerBase], CompilerBase], Dict[str, Callable[[Any], Any]]] = None
    add_fields: Dict[str, Any] = None
    
    async def generate_regex101_link(self, text: str) -> str:
        query_params = dict \
        (
            regex = self.regex.pattern,
            testString = text,
            flags = self.regex.flags,
            delimiter = "'",
            flavor = 'python',
        )
        
        host = 'https://regex101.com'
        query = urlparse.urlencode(query_params)
        url = f'{host}/?{query}'
        return url
    
    def parser_kwargs(self) -> Dict[str, Any]:
        kw = dict()
        for f in fields(self): # type: Field
            kw[f.name] = getattr(self, f.name)
        
        return kw
    
    async def parse(self, text: str, logger: Logger) -> T:
        try:
            return parse_raw_data(text_data=text, logger=logger, **self.parser_kwargs())
        except CannotParseTextException as e:
            link = await self.generate_regex101_link(text)
            raise ParserError(e, link)
        except TextToRawException as e:
            raise ParserError(e)

__all__ = \
[
    'ParserError',
    'ParserOptions',
]
