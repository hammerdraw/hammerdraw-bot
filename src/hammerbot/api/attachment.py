import os
from dataclasses import dataclass
from tempfile import mkstemp
from typing import *
from uuid import uuid4

from botter.api import Attachment, AttachmentType
from botter.util.os_operations import safe_remove
from hammerdraw.compilers import CompilerBase

@dataclass(frozen=True)
class AttachmentOptions:
    extension: str = '.png'
    forced_width: Optional[int] = 720

T = TypeVar('T', bound=CompilerBase)
# T = CompilerBase
class HammerDrawAttachment(Attachment, Generic[T]):
    instance: T
    options: AttachmentOptions
    
    def __init__(self, instance: T, options: AttachmentOptions):
        self.instance = instance
        self.options = options
        
        _filename = self.instance.get_output_name() + self.options.extension
        super().__init__ \
        (
            id = str(uuid4()),
            type = AttachmentType.Image,
            filename = _filename
        )
    
    @property
    async def temp_file(self) -> str:
        fd, path = mkstemp(suffix='.' + self.filename)
        with os.fdopen(fd, 'wb'):
            pass
        
        old_name = self.instance.compiled_filename
        self.instance.compiled_filename = os.path.abspath(path)
        result = self.instance.save_compiled(forced_width=self.options.forced_width)
        if (path != result):
            safe_remove(path)
        self.instance.compiled_filename = old_name
        return result
    
    @property
    async def content(self) -> bytes:
        with open(await self.temp_file, 'rb') as f:
           return f.read()

__all__ = \
[
    'AttachmentOptions',
    'HammerDrawAttachment',
]
