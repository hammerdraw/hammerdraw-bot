DEST_DIR=/opt/hammerdraw
PYTHON_VERSION=python3.7
VIRTUAL_ENV_PATH=${DEST_DIR}/.venv
CONFIG_DIR=/opt/hammerdraw/hammerdraw-bot/resources

USER=hammerdraw
HAMMERDRAW_VERSION=dev
HAMMERBOT_VERSION=master
MODULES=(keystone warhammer-quest)
BACKENDS=(telegram discord)
UNIT_NAME=hammerbot.service
