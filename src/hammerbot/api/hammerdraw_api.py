import json
from os import fdopen, remove
from tempfile import mkstemp
from typing import *

from dataclasses import replace

from botter.api import *
from botter.api.handlers import *
from hammerbot.util import ImageSearcher, LinkProvider
from hammerdraw.config import HammerDrawConfig
from hammerdraw.compilers import CompilerBase
from hammerdraw.interfaces import ILoggable
from hammerdraw.util import ImageType
from .attachment import *
from .parser_api import ParserOptions, ParserError

class HammerDrawError(BotError):
    pass

T = TypeVar('T', bound=CompilerBase)
class HammerDrawApi(ReplyHandler, ILoggable, Generic[T]):
    parser_options: ParserOptions[T]
    attachment_options: AttachmentOptions
    hammerdraw_config: HammerDrawConfig
    link_provider: LinkProvider
    
    async def can_handle(self, event: MessageEvent) -> bool:
        return await super().can_handle(event) \
           and await self._can_handle__check_size(event)
    
    async def _can_handle__check_size(self, event: MessageEvent) -> bool:
        text = event.message.text
        if (event.message.mentions):
            text = '\n'.join(text.splitlines()[1:])
        
        return len(text.splitlines()) >= 3
    
    async def parse(self, text: str, **kwargs) -> T:
        self.logger.debug("Parsing text data...")
        return await self._parse(text, **kwargs)
    async def _parse(self, text: str, **kwargs) -> T:
        return await replace(self.parser_options, update_fields=lambda t: self.update(t, **kwargs)).parse(text, self.logger)
    
    def update(self, instance: T, **kwargs) -> T:
        self.logger.debug("Updating parsed data...")
        
        _update = self.parser_options.update_fields
        if (not _update):
            pass
        elif (callable(_update)):
            instance = _update(instance)
        else:
            from hammerdraw.util.text_to_raw.base import map_field
            for key, func in (_update or {}).items():
                map_field(instance, key, func, only_str_fields=False)
        
        return self._update(instance, **kwargs)
    def _update(self, instance: T, **kwargs) -> T:
        return instance
    
    async def compile(self, instance: T, **kwargs) -> ImageType:
        self.logger.debug("Compiling image...")
        return await self._compile(instance, **kwargs)
    async def _compile(self, instance: T, **kwargs) -> ImageType:
        result = instance.compile()
        if (not result):
            raise HammerDrawError("Cannot compile image")
        return result
    
    async def generate_image(self, text: str, **kwargs) -> Tuple[T, ImageType]:
        instance = await self.parse(text, **kwargs)
        image = await self.compile(instance, **kwargs)
        return instance, image
    
    async def handle_message(self, message: InboundMessage, **kwargs) -> Optional[Message]:
        text = message.text
        if (message.mentions):
            text = '\n'.join(text.splitlines()[1:])
        
        try:
            instance, image = await self.generate_image(text, **kwargs)
        except BotError as e:
            if (len(text.splitlines()) < 3):
                return None
            else:
                if (isinstance(e, ParserError) and e.link):
                    new_link = await self.link_provider.provide_external_link(e.link)
                    e = ParserError(e.base_exception, new_link)
                print(f"{e!r}")
                return Message(f"Cannot generate image for you\n{e}")
        except Exception as e:
            msg = f"Internal error occurred\n{e}"
            self.logger.exception(msg)
            return Message(msg)
        else:
            return Message("", [ HammerDrawAttachment(instance, self.attachment_options) ])

class HammerDrawApiWithImage(HammerDrawApi[T], Generic[T]):
    image_searcher: Optional[ImageSearcher] = None
    topic: str = None
    
    def _update(self, instance: T, **kwargs) -> T:
        instance.raw['image'] = kwargs.pop('image', None)
        self.logger.info(json.dumps(instance.raw, indent=4, sort_keys=True))
        return super()._update(instance, **kwargs)
    
    async def handle_message(self, message: InboundMessage, **kwargs) -> Message:
        image = None
        img_content: Optional[bytes] = None
        img_extension: Optional[str] = None
        
        if (message.has_image):
            img = next(at for at in message.attachments if at.type == AttachmentType.Image)
            img_content = await img.content
            img_extension = img.filename
        
        elif (self.image_searcher is not None):
            text = message.text
            if (message.mentions):
                text = '\n'.join(text.splitlines()[1:])
            
            query = list()
            if (self.topic):
                query.append(self.topic)
            
            _t = text.splitlines(False)
            if (_t): query.append(_t[0])
            query.append('artwork') # Seriously
            img_content, img_extension = await self.image_searcher.get_random_image(' '.join(query))
        
        if (img_content is not None):
            fd, image = mkstemp(suffix='.' + img_extension)
            with fdopen(fd, 'wb') as file:
                file.write(img_content)
            del img_content
        
        result = await super().handle_message(message, image=image)
        if (img_extension is not None):
            remove(image)
        return result
del T

__all__ = \
[
    'HammerDrawApi',
    'HammerDrawApiWithImage',
    'HammerDrawError',
]
