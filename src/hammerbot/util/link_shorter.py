from asyncio import get_event_loop, AbstractEventLoop

from bitlyshortener import Shortener

from hammerbot.util.link_provider_basic import BasicLinkProvider

class BitLyLinkShortener(Shortener):
    default_domain: str = 'https://bit.ly'
    
    def _int_id_to_short_url(self, url_id: int):
        url_id_ = self._bytes_int_encoder.decode(url_id).decode()
        short_url = f"{self.default_domain}/{url_id_}"
        return short_url

class BitLyLinkProvider(BasicLinkProvider):
    bitly_token: str
    bitly_client: Shortener
    loop: AbstractEventLoop
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.bitly_token = self.config.extra_tokens.get('bitly_token')
        if (self.bitly_token is None):
            raise ValueError(f"BitLy token is not provided for the {BitLyLinkProvider.__name__} class")
        
        self.bitly_client = BitLyLinkShortener(tokens=[ self.bitly_token ])
        self.loop = get_event_loop
    
    async def provide_external_link(self, url: str) -> str:
        data = self.bitly_client.shorten_urls([ url ])
        return data[0]


__all__ = \
[
    'BitLyLinkProvider',
    'BitLyLinkShortener',
]


import asyncio

async def main():
    from hammerbot.config import HammerBotConfig
    client = BitLyLinkProvider(HammerBotConfig.default().bot)
    
    links = \
    [
        'https://example.com',
        'https://regex101.com/?regex=%28%3FP%3Cname%3E.%2B%29%0A%28%3FP%3Crarity%3E.%2B%29%0A%28%3FP%3Crace%3E%5Cw%2B%29+%28%3FP%3Ctype%3E.%2B%29%0A%28%3FP%3Cminerals%3E%5Cd%2B%29+%2A%5B%2F+%5D+%2A%28%3FP%3Cgas%3E%5Cd%2B%29%0A%28%3F%3Aset%3A+%2A%28%3FP%3Cset%3E%5B%5E%5Cn%5D%2B%29%24%0A%29%3F%0A%28%3FP%3Ceffect%3E%28.%7C%5Cs%29%2A%29&testString=Potentially+Wrong+Card%0ATerran+Action%0A2+%2F+2%0A%0AThis+card+misses+the+rarity.&flags=40&delimiter=%27&flavor=python',
    ]
    for l in links:
        r = await client.provide_external_link(l)
        print(r)
    
    return 0

if (__name__ == '__main__'):
    exit_code = asyncio.run(main())
    exit(exit_code)
