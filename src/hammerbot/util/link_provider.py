from abc import ABC

class LinkProvider(ABC):
    """
    A class that provides link shorting.
    Used to provide links to both internal and external resources.
    """
    
    async def provide_external_link(self, url: str) -> str:
        """
        Provide a link to an external resource.
        Usually this returns shortened link.
        
        Args:
            url: `str`: The URL to an external resource.
        
        Returns:
            `str`: Returns a link that is resolved into the given URL.
        """
        
        pass
    
    async def provide_internal_link(self, uri: str) -> str:
        """
        Provide a link to an internal resource.
        Usually this returns shortened link.
        
        Args:
            uri: `str`: The URI to an internal resource.
        
        Returns:
            `str`: Returns a link that is resolved into the given URL.
        """
        
        pass


__all__ = \
[
    'LinkProvider',
]
