#!/usr/bin/env bash
set -eu

cd "$(dirname "$(realpath "$0")")";
source ./config.sh

function install-package()
{
    local package=${1:?"Required parameter 'package' is missing"}
    local version=${2:?"Required parameter 'version' is missing"}
    
    local dir_name="${package}-${version}"
    local archive_name="${dir_name}.tar.gz"
    local url="https://gitlab.com/hammerdraw/${package}/-/archive/${version}/${archive_name}"
    wget -v ${url}
    tar -xvf ${archive_name}
    rm -vf ${archive_name}
    
    pushd ${dir_name}
        pip install --no-cache-dir -r requirements.txt
        pip install --no-cache-dir -e .
    popd
    
    ln -svf ${dir_name} ${package}
}

function install-backend()
{
    local backend=${1:?"Required parameter 'backend' is missing"}
    local config=${2:?"Required parameter 'config' is missing"}
    local token_var_name="SECRETS_${backend^^}_TOKEN"
    
    pip install "botter[${backend}]"
    echo "secret_tokens.${backend}: \"${!token_var_name:="${backend^} token is empty"}\"" >> ${config}  
}

pushd ${DEST_DIR}
    virtualenv -vp ${PYTHON_VERSION} ${VIRTUAL_ENV_PATH}
    set +u
    source ${VIRTUAL_ENV_PATH}/bin/activate
    set -u
    
    install-package hammerdraw-core ${HAMMERDRAW_VERSION}
    install-package hammerdraw-bot ${HAMMERBOT_VERSION}
    
    pushd hammerdraw-core
        for MODULE in ${MODULES[@]}
        do python src/scripts/install-module.py ${MODULE}
        done
    popd
    
    SECRETS=$(mktemp -t secrets.XXXXXXXXXX.conf)
    for BACKEND in ${BACKENDS[@]}
    do install-backend ${BACKEND} ${SECRETS}
    done
    install -vm 400 -o ${USER} -g ${USER} ${SECRETS} ${CONFIG_DIR}/secret-tokens.conf
    rm ${SECRETS}
    
    install -vm 755 -d hammerdraw-core/logs/
popd
