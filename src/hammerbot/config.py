from typing import *

from dataclasses import dataclass, field

import botter.api.handlers
import hammerbot.api.hammerdraw_api
import hammerbot.util.image_searcher
from dataclasses_config import *
from hammerdraw.config import HammerDrawConfig

@dataclass(frozen=Settings.frozen)
class BotConfig(Config):
    handler_class: DynamicClass[hammerbot.api.HammerDrawApi]
    provider: DynamicClass[botter.api.BotImpl]
    link_provider: DynamicClass[hammerbot.util.LinkProvider]
    image_search_engine: Optional[DynamicClass[hammerbot.util.image_searcher.ImageSearcher]]
    filter: Optional[DynamicClass[botter.api.handlers.EventHandler]]
    dev_features: bool
    
    token: str
    args: Dict[str, Any] = field(default_factory=dict)
    attachment_options: Dict[str, Any] = field(default_factory=dict)
    extra_tokens: Dict[str, str] = field(default_factory=dict)

@main_config(root_config_path='hammerbot')
class HammerBotConfig(MainConfig):
    hammerdraw: HammerDrawConfig
    bot: BotConfig
    
    pid_file: str

__all__ = \
[
    'BotConfig',
    'HammerBotConfig',
    'HammerDrawConfig',
]

# region Debug
def main():
    print(Path('.'))
    print(Path('../hammerdraw-core'))
    config = HammerBotConfig.default()
    print(config)
    print(config.hammerdraw)
    print(config.bot)
    print(config.bot.handler_class)
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
# endregion
