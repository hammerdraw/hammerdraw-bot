from typing import *
from urllib.parse import urlparse

from botter import Message, AttachmentType, InMemoryAttachment
from botter.api.handlers import *
from hammerbot.util import ImageSearcher

from .base import BaseHandler

class CleanupCommandHandler(BaseHandler, CommandHandler):
    command: str = 'cleanup'
    num_args: int = 1
    
    # noinspection PyMethodOverriding
    async def handle_command(self, event: MessageEvent, command: str, count: str):
        from botter.discord import DiscordUser, DiscordMessage
        count = int(count)
        if (self.config.dev_features and isinstance(event.bot_user, DiscordUser) and isinstance(event.message, DiscordMessage)):
            c = event.message.channel.discord_channel
            async for m in c.history(limit=count):
                await c.delete_messages([ m ])
        else:
            await self.reply_error(event, "This functionality is disabled")

class DeleteCommandHandler(CommandHandler):
    command: str = 'delete'
    
    async def is_url(self, s: str) -> bool:
        try:
            p = urlparse(s, allow_fragments=False)
        except:
            return False
        else:
            return p and len(p) > 6
    
    async def handle_command(self, event: MessageEvent, command: str, *message_ids: str):
        try:
            await event.message.channel.send_typing()
        except NotImplementedError as e:
            self.logger.warning("Typing not supported")
        
        for message_id in message_ids:
            if (await self.is_url(message_id)):
                message_id = message_id.rpartition('/')[-1]
            
            self.logger.debug(f"Requested to delete message #{message_id}")
            try:
                msg = await event.message.channel.fetch_message(message_id)
            except (event.base_error, ValueError):
                await self.reply_error(event, f"Cannot delete message #{message_id}: Unable to find the specified message")
            else:
                if (msg.author != event.bot_user):
                    await self.reply_error(event, f"Cannot delete message #{message_id}: Not owned by bot")
                elif (event.message.author not in msg.mentions):
                    await self.reply_error(event, f"Cannot delete message #{message_id}: Reply not to you")
                else:
                    try:
                        await msg.delete()
                    except event.base_error as e:
                        self.logger.error(f"Cannot delete message #{message_id}: Generic error: {e}")

class SearchCommandHandler(BaseHandler, CommandHandler):
    command: str = 'search'
    image_searcher: Optional[ImageSearcher] = None
    
    def __init__(self, *args, **kwargs):
        # pass
        super().__init__(*args, **kwargs)
        if (self.config.image_search_engine is not None):
            self.image_searcher = self.config.image_search_engine.cls()
    
    async def handle_command(self, event: MessageEvent, command: str, *query: str):
        try:
            await event.message.channel.send_typing()
        except NotImplementedError as e:
            self.logger.warning("Typing not supported")
        
        if (self.image_searcher is None):
            return await self.reply_error(event, "Image searcher is disabled")
        
        img, ext = await self.image_searcher.get_random_image(' '.join(query))
        if (not img):
            return await self.reply_error(event, "Unable to find any image")
        
        at = InMemoryAttachment(extension=ext, _content=img, type=AttachmentType.Image)
        self.logger.info("Replying image...")
        return await event.message.reply(Message('', attachments=[ at ]))

ALL_COMMAND_HANDLERS = \
[
    CleanupCommandHandler,
    DeleteCommandHandler,
    SearchCommandHandler,
]


__all__ = \
[
    'ALL_COMMAND_HANDLERS',
    
    'CleanupCommandHandler',
    'DeleteCommandHandler',
    'SearchCommandHandler',
]
