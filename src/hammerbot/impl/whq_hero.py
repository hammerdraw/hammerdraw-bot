import re

from typing.re import *

from hammerdraw.modules.warhammer_quest import HeroCompiler
from ..api.hammerdraw_api import HammerDrawApiWithImage
from ..api.parser_api import ParserOptions

class WarhammerQuestHeroApi(HammerDrawApiWithImage[HeroCompiler]):
    logger_name = 'hammerbot.impl.whq-hero'
    topic: str = "Warhammer"
    regex: Pattern
    
    def __init__(self):
        super().__init__()
        
        self.regex = re.compile(r'''(?P<name>.+?)( +\((?P<subtitle>.+)\))?
(?P<stats>(?P<stats_move>\d+) *[/ ] *(?P<stats_agility>\d+)\+? *[/ ] *(?P<stats_save>\d+)\+?)
(?P<traits>(?:(?P<trait>\w+),? *)+)
(?i:renown):(?P<renown>(.|\s)*?)

(?P<weapons>(?P<weapon>^(?P<weapon_name>[^:\n]+?) +\((?P<weapon_cost>\d+)\+?\) +(?P<weapon_range>\w+) +(?P<weapon_hit>\d+)\+ +(?P<weapon_damage>[^\n]+)(?:\n|$))+)

(?P<abilities>(?P<ability>^(?P<ability_name>[^:\n]+?)(?: *\((?P<ability_cost>\d+)\+?(?P<ability_diceSpace>, +dice)?\))?: *(?P<ability_description>(?:.+(?:\n|$))*)(?:\n|$))*)''', re.MULTILINE)
        self.parser_options = ParserOptions \
        (
            compiler = HeroCompiler,
            compiler_args = dict(config=self.hammerdraw_config),
            regex = self.regex,
            multiline_fields = [ 'renown', 'abilities.[ability].description' ],
            expand_fields = [ 'image' ],
            decapitalize_fields = [ 'traits.[trait]', 'weapons.[weapon].range' ],
            numeric_fields = [ 'stats.move', 'stats.agility', 'stats.save', 'weapons.[weapon].cost', 'weapons.[weapon].hit', 'abilities.[ability].cost' ],
            list_fields = dict(abilities='ability', traits='trait', weapons='weapon'),
            structure_fields =
            {
                'abilities.[ability]': ('ability', [ 'name', 'cost', 'description', 'diceSpace' ]),
                'weapons.[weapon]': ('weapon', [ 'name', 'cost', 'range', 'hit', 'damage' ]),
                'stats': ('stats', [ 'move', 'agility', 'save' ]),
            },
            # add_fields =
            # {
            #     'image': 'images/{filename}.jpg',
            # },
            update_fields =
            {
                'abilities.[ability].diceSpace': lambda s: bool(s),
            },
        )
    
    def _update(self, instance: HeroCompiler, **kwargs) -> HeroCompiler:
        instance.raw['autoAdjustScale'] = True
        return super()._update(instance, **kwargs)

__all__ = \
[
    'WarhammerQuestHeroApi',
]
